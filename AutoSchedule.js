// ==UserScript==
// @name         AutoSchedule
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Automatisch naar het juiste rooster
// @author       Bram de Smidt
// @match        http://roosters.carmelcollegesalland.nl/havo-vwo/leerlingenrooster/menu.html
// @grant        none
// @downloadURL  https://gitlab.com/bramw2003/Tampermonkey/raw/master/AutoSchedule.js
// @updateURL    https://gitlab.com/bramw2003/Tampermonkey/raw/master/AutoSchedule.js
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';
    var aTags = document.getElementsByTagName("a");
    var searchText = "21202";
    var found;

    for (var i = 0; i < aTags.length; i++) {
        //if (aTags[i].innerHTML == searchText) {
        //    found = aTags[i];
        //    break;
        //}
        if(aTags[i].innerHTML.indexOf(searchText) !== -1) {
            // something
            found = aTags[i];
            break;
        }
    }
    found.click();
    // Your code here...
})();