// ==UserScript==
// @name         SomToday Redesign (Login Screen)
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       Bram de Smidt
// @match        https://somtoday.nl/*
// @grant        none
// @updateURL    https://gitlab.com/bramw2003/Tampermonkey/raw/master/SomToday%20Redesign%20(Login%20Screen).js
// @downloadURL  https://gitlab.com/bramw2003/Tampermonkey/raw/master/SomToday%20Redesign%20(Login%20Screen).js
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';
    var elements = document.querySelectorAll('.stpanel');
    for(var i=0; i<elements.length; i++){
        elements[i].style.background = "url('https://i.ytimg.com/vi/FrSGRD3XgXI/maxresdefault.jpg')";
        elements[0].style.color = "red";

        var d = new Date();
        var newText = document.createElement('h1'); // create new H3
        var text = document.createTextNode(d.getHours() + ":" + d.getMinutes());
        newText.appendChild(text);
        var el = document.querySelectorAll('section');
        for(var a =0;a<el.length; a++){
            el[a].appendChild(newText); // add it to the div
        }
        el = document.querySelectorAll('h1');
        for(a =0;a<el.length; a++){
            el[a].style.textAlign = "center";
            el[a].style.color = 'black';
        }
        el = document.querySelectorAll('ul.stpanel--links');
        for(a =0;a<el.length; a++){
            el[a].style.background = "rgba(255, 255, 255, 0)";
            el[a].style.color = 'black';
        }
        
        document.body.style.background = "url('https://i.ytimg.com/vi/FrSGRD3XgXI/maxresdefault.jpg')";
        document.body.style.backgroundSize = "cover";
        document.body.style.backgroundAttachment = "fixed";
        document.dody.style.backgroundPosition = 'center';
    }
    // Your code here...
})();