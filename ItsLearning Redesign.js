// ==UserScript==
// @name         ItsLearning Redesign
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  Change the default design of It's learning with something more attractive
// @author       Bram de Smidt
// @match        https://ccs.itslearning.com/
// @match        https://ccs.itslearning.com/index.aspx
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.getElementById('ctl00_ContentPlaceHolder1_News_NewsComponent').remove();
    document.getElementById('ctl00_language_main').remove();
    var elements = document.querySelectorAll('.l-login-links');
    for(var i=0; i<elements.length; i++){
        elements[i].remove();
    }
    elements = document.querySelectorAll('.copyright');
    for(i=0; i<elements.length; i++){
        elements[i].remove();
    }
    elements = document.querySelectorAll('img');
    for(i=0; i<elements.length; i++){
        elements[i].remove();
    }
    elements = document.querySelectorAll('h1.h-dsp-ib.h-mr0');
    for(i=0; i<elements.length; i++){
        elements[i].remove();
    }
    elements = document.querySelectorAll('p.h-fnt-sm');
    for(i=0; i<elements.length; i++){
        elements[i].remove();
    }
    elements = document.querySelectorAll('.itsl-login');
    for(i=0; i<elements.length; i++){
        elements[i].style.background = "url('https://i.ytimg.com/vi/FrSGRD3XgXI/maxresdefault.jpg')";
        elements[i].style.backgroundPosition = "center";
        elements[i].style.backgroundSize = "cover";
        elements[i].style.backgroundAttachment = "fixed";
    }

    var d = new Date();
    document.getElementById('ctl00_ContentPlaceHolder1_LoginSection').style.backgroundColor = "rgba(255, 255, 255, 0.4)";
    document.getElementById('ctl00_ContentPlaceHolder1_LoginSection').style.boxShadow = "0 1px 2px 0 rgba(0,0,0,0)";
    document.getElementById('ctl00_ContentPlaceHolder1_LoginSection').style.border = "1px solid rgba(0,0,0,0)";
    document.getElementById('ctl00_ContentPlaceHolder1_LoginSection').style.color = "black";
    document.getElementById('ctl00_ContentPlaceHolder1_nativeLoginButton').style.borderColor = "#4cae4c";
    var login = document.getElementById('ctl00_ContentPlaceHolder1_nativeAndLdapLogin');
    document.getElementById('ctl00_ContentPlaceHolder1_nativeAndLdapLogin').innerHTML = "<h1>" + d.getHours() + ":" + d.getMinutes()+ "</h1>" + document.getElementById('ctl00_ContentPlaceHolder1_nativeAndLdapLogin').innerHTML;
    elements = document.querySelectorAll('a');
    for(i=0; i<elements.length; i++){
        elements[i].style.color = "black";
    }
            var d = new Date();
        var newText = document.createElement('h1'); // create new H3
        var text = document.createTextNode(d.getHours() + ":" + d.getMinutes());
        newText.appendChild(text);
        var el = document.querySelectorAll('body');
        for(var a =0;a<el.length; a++){
            el[a].appendChild(newText); // add it to the div
        }
        el = document.querySelectorAll('h1');
        for(a =0;a<el.length; a++){
            el[a].style.textAlign = "center";
            el[a].style.color = 'black';
        }
    // Your code here...
})();
