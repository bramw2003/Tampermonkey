// ==UserScript==
// @name         Somtoday Redesign (After login)
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  try to take over the world!
// @author       Bram de Smidt
// @match        https://carmelcollegesalland-elo.somtoday.nl/home/news*
// @match        https://carmelcollegesalland-elo.somtoday.nl/home/*
// @updateURL    https://gitlab.com/bramw2003/Tampermonkey/raw/master/Somtoday%20Redesign%20(After%20login).js
// @downloadURL  https://gitlab.com/bramw2003/Tampermonkey/raw/master/Somtoday%20Redesign%20(After%20login).js
// @run-at document-end
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var ImageUrl = "https://i.ytimg.com/vi/FrSGRD3XgXI/maxresdefault.jpg"

    document.body.style.background = "url('" + ImageUrl + "')";
    document.body.style.backgroundPosition = "center";
    document.body.style.backgroundSize = "cover";
    document.body.style.backgroundAttachment = "fixed";
    var el = document.querySelectorAll('div.date');
    for(var a =0;a<el.length; a++){
        el[a].remove();
    }
    el = document.querySelectorAll('div.version');
    for(a =0;a<el.length; a++){
        el[a].remove();
    }
    el = document.querySelectorAll('img');
        for(a =0;a<el.length; a++){
            el[a].remove();
        }
    //document.getElementById("#343").remove();
    // Your code here...
})();